jQuery(document).ready(function($) {

    $( "#product-tabs" ).tabs();

    $('.owl-carousel').owlCarousel({
        margin: 60,
        autoWidth:true,
        items: 4,
        nav: true,
        loop:true,
        mouseDrag:false,
        dots: false,
        responsiveClass: true,
        responsive:{
            0:{
                items:2,
                margin: 14,
            },
            551:{
                items:3,
                margin: 30,
            },
            1000:{
                items:3,
                margin: 40,
            },
            1400:{
                items:4,
                margin: 60,
            }
        }
    });

    $('#menuToggle').click(function(event) {
        if( $(this).hasClass('active')) {
            $(this).removeClass('active');
            $('#header-block').removeClass('menu--open');
        }else {
            $('#header-block').addClass('menu--open');
            $(this).addClass('active');
        }
    });
});
